<!DOCTYPE HTML>
<html lang='en'>
    <head>
        <title>Mockup Design</title>
        <style>
        	* {
        		font-family: Arial; }
            body {
                margin: 0;
                overflow: hidden; }
                .nav-toggle {
                    position: fixed;
                    z-index: 2;
                    width: 50px;
                    height: 100vh;
                    background-color: transparent; }
                .nav {
                    transition: 0.5s;
                    background: #f1f1f1;
                    display: inline-block;
                    padding: 20px;
                    opacity: 0;
                    z-index: 1;
                    position: fixed; }
                    .nav.active {
                        opacity: 1; }
                    h1 {
                        font-weight: normal;
                        font-size: 24px;
                        margin: 0;
                        margin-left: 25px;
                    }
                    ol {
                        padding: 0;
                        padding-left: 25px;
                        padding-right: 20px;
                        overflow-y: auto; }
                        ol::-webkit-scrollbar {
                            width: 10px;
                            background: hsla(0,0%,100%,.2);
                            border-radius: 10px;
                        }
                        ol::-webkit-scrollbar-thumb {
                            border-radius: 10px;
                            background: #d4d4d4;
                        }
                        ol::-webkit-scrollbar-track {
                            border-radius: 10px;
                        }
                        a {
                            text-decoration: none; }
                            li {
                                padding: 5px 0;
                                transition: 0.5s;
                                color: #000;
                                font-size: 15px; }
                                li:visited {
                                    color: #111; }
                                li:hover {
                                    opacity: 0.7; }
                iframe {
                    min-width: 100vw;
                    display: inline-block;
                    height: 100vh;
                    position: fixed;
                    right: 0;
                    top: 0;
                    border: 0;
                    background: white;
                    z-index: 0; }
        </style>
    </head>
    <body>
        <div class="nav-toggle" onmouseover="opennav()"></div>
        <div class="nav active" id="nav">
            <h1>Mockup Designs</h1>
            <?php
                $default_page_link = "";
                function listFolderFiles($dir) {
                    echo '<ol>';
                    $index = -2;
                    foreach (new DirectoryIterator($dir) as $fileInfo) {
                        if (!$fileInfo->isDot() && !(($fileInfo->getFilename())=="index.php") && strpos($fileInfo->getFilename(), ".php")==false && !($fileInfo->getFilename()=="generated_html") && (strpos($fileInfo->getFilename(), ".jpg")==true || strpos($fileInfo->getFilename(), ".png")==true)) {
                            $myfile = fopen("generated_html/" . $fileInfo->getFilename() . ".php" , "w") or die("Unable to open file!");
                            $myfilecontent = "
                                <!DOCTYPE HTML>
                                <html lang='en'>
                                    <head>
                                        <title>Mockup Design</title>
                                    </head>
                                    <body style='text-align: center; margin: 0;'>
                                    <img id='img_mock' style='max-width: 100%;' src='../mockup_designs/" . $fileInfo->getFilename() . "'/>
                                    <script>
                                    var filename = '" . $fileInfo->getFilename() . "';
                                    if (filename.search('obile') >= 0) {
                                        document.getElementById('img_mock').style.width = '425px';
                                    }
                                    </script>
                                    </body>
                                </html>
                                ";
                            fwrite($myfile, $myfilecontent);
                            echo '<a style="margin: 0 auto; display: inline;" onclick="togglenav()" href="generated_html/' . $fileInfo->getFilename() . '.php" target="mockup"><li>' . $fileInfo->getFilename() . '</li></a>';
                            if ( $GLOBALS['default_page_link'] == "" ) $GLOBALS['default_page_link'] = 'generated_html/' . $fileInfo->getFilename() . '.php';
                        }
                        elseif ( is_dir( $dir . "/" . $fileInfo->getFilename() ) ) {
                            echo '<li>' . $fileInfo->getFilename();
                            //listFolderFiles( $dir . "/" . $fileInfo->getFilename() );
                            echo '</li>';
                        }
                        $index++;
                    }
                    echo '</ol>';
                }
                listFolderFiles(getcwd() . "/mockup_designs");
            ?>
        </div>
        <iframe onmouseover="closenav()" src="<?php echo $default_page_link; ?>" name="mockup"></iframe>
        <script type="text/javascript">
            elementnav = document.getElementById('nav');
            function togglenav() {
                if ( hasClass( elementnav, 'active') ) elementnav.classList.remove('active');
                else elementnav.classList.add('active');
            }
            function opennav() {
                elementnav.classList.add('active');
            }
            function closenav() {
                elementnav.classList.remove('active');
            }
            function hasClass(element, cls) {
                return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
            }
        </script>
    </body>
</html>